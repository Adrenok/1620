import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is a simple program that will let you practice with exceptions.
 * 
 * 
 * Unfortunately, working with numbers can be problematic, and there are two exceptional cases with this code.
 * The first is if the user doesn't entered a number that is formatted correctly.
 * The second is if the user sets the denominator to 0. This leads to a divide by 0 error.
 * 
 * Here is an example of problematic input from the user:
 *  
 
Welcome to the division program.
Enter the first number (numerator) as an integer
10
Enter the second number (denominator) as an integer
0
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at ExcetpionMain.main(ExcetpionMain.java:20)


 * Notice that entering a zero as the denominator threw an arithmetic exception.
 * 
 * Here is another example of problematic input: 
 * 
Welcome to the division program.
Enter the first number (numerator) as an integer
10
Enter the second number (denominator) as an integer
five
Exception in thread "main" java.util.InputMismatchException
	at java.util.Scanner.throwFor(Unknown Source)
	at java.util.Scanner.next(Unknown Source)
	at java.util.Scanner.nextInt(Unknown Source)
	at java.util.Scanner.nextInt(Unknown Source)
	at ExcetpionMain.main(ExcetpionMain.java:41)
	
 * The goal of this homework is to catch both of these possible exceptions and handle them appropriately.
 * 
 * In order to get full credit, you must use try and catch the deal with both problems.
 * (Notably, you can handle the division by zero problem by having an if statement that looks for denominator with a value of zero. for this assignment, do not use this approach.)
 * 
 * Using try and catch blocks, update this program so that the output would be as follows in the two following scenarios:
 * 
 * The following two constants might be helpful to make sure that you have the exactly right output.
 * 
private static final String DIVIDE_BY_ZERO = "You can't divide by zero. Please run the program again.";
private static final String INVALID_NUMBER = "You entered an invalid number (remember they have to be integers). Please run the program again.";


Example One:

Welcome to the division program.
Enter the first number (numerator) as an integer
10
Enter the second number (denominator) as an integer
Five
You entered an invalid number (remember they have to be integers). Please run the program again.

Example Two:

Welcome to the division program.
Enter the first number (numerator) as an integer
10
Enter the second number (denominator) as an integer
0
You can't divide by zero. Please run the program again.

Example Three:

Welcome to the division program.
Enter the first number (numerator) as an integer
10
Enter the second number (denominator) as an integer
5
The quotient is 2.0

You *should* remove this comment in your final submission.
 * 
 * 
 * @author bricks
 *
 */
public class ExceptionMain extends Exception {
	
	private static final String INVALID_NUMBER = "You entered an invalid number (remember they have to be integers). Please run the program again.";
	
	private static final String DIVIDE_BY_ZERO = "You can't divide by zero. Please run the program again.";
	
	public static void main(String[] args) {
		
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Welcome to the division program.");
		
		System.out.println("Enter the first number (numerator) as an integer");
		
		int numerator = scanner.nextInt();
		
		System.out.println("Enter the second number (denominator) as an integer");
		
		
		
		int denominator = scanner.nextInt();
		
		double quotient = 0.0;
		
		try 
		{
			quotient = numerator/denominator;
			System.out.println("The quotient is " + quotient);
		}
		catch ( ArithmeticException aException )
		{
			System.out.println(DIVIDE_BY_ZERO);
		}
		catch ( InputMismatchException iException )
		{
			System.out.println(INVALID_NUMBER);
		}
	}
}