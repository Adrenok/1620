/**
 * This interface should contain exactly one method.
 * It should be called feed.
 * It shouldn't return anything.
 * It should take as a parameter, a reference called food of type IFood
 * @author bricks
 *
 */
public interface IAnimal {
	
	/**
	 * This method must be implemented by any class which implements IAnimal.
	 * @param food a type of food
	 */
	
	void feed( IFood food );
	
}
