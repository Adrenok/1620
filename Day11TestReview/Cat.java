/**
 * This class represents a cat.
 * 
 * Cat should inherit from AAnimal.
 * 
 * Cat should override the methods necessary to make this a concrete class.
 * 
 * A cats happiness improves only if it is fed cat food.
 * 
 * @author bricks
 *
 */

public class Cat extends AAnimal {

	/**
	 * The constructor doesn't need to initialize any data members.
	 */
	
	public Cat()
	{
		
	}
	
	/**
	 * This method feeds the Cat food which should be of a type IFood which is any food. It then
	 * increases the happiness value of the Cat by one if and only if it is of type CatFood.
	 */
	
	public void feed( IFood food )
	{
		if ( food instanceof CatFood )
		{
			improveHappiness();
		}
	}

}
