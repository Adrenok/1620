/**
 * 
 */

/**
 * @author bricks
 * 
 * This is the main file for a project.
 * 
 * You should not have to change anything in this file.
 * 
 * Here's the expected output of the program:

Welcome to the farm!!! It's smells great!
Let's feed some animals some food and see what happens.

Let's see how happy our animals are:
The Cat has a happiness value of 0
The Goat has a happiness value of 0

Okay, let's feed our animals some cat food.

Let's see how happy our animals are:
The Cat has a happiness value of 1
The Goat has a happiness value of 1

Okay, let's feed our animals some what we'd normally put down the disposal.

Let's see how happy our animals are:
The Cat has a happiness value of 1
The Goat has a happiness value of 2


 *
 */
public class Main {

	static IAnimal cat = new Cat();
	static IAnimal goat = new Goat();
	
	public static void main(String[] args) {
		
		
		System.out.println("Welcome to the farm!!! It's smells great!");
		System.out.println("Let's feed some animals some food and see what happens.");
		
		checkHappiness();
		
		feedCatFood();
		
		checkHappiness();
		
		foodVegetableLeftOvers();
		
		checkHappiness();
		

	}

	private static void foodVegetableLeftOvers() {
		System.out.println();
		System.out.println("Okay, let's feed our animals some what we'd normally put down the disposal.");
		cat.feed(new VegetableLeftOvers());
		goat.feed(new VegetableLeftOvers());
		
	}

	private static void feedCatFood() {
		
		System.out.println();
		System.out.println("Okay, let's feed our animals some cat food.");
		cat.feed(new CatFood());
		goat.feed(new CatFood());
		
	}

	private static void checkHappiness() {
		System.out.println();
		System.out.println("Let's see how happy our animals are:");
		System.out.println(cat);
		System.out.println(goat);
		
	}

}
