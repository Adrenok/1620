/**
 * This class represents a goat.
 * 
 * Goat should inherit from AAnimal.
 * 
 * You should have the methods required to make this a concrete class.
 * 
 * Goat improves it happiness no matter what it is fed.
 * @author bricks
 *
 */
public class Goat extends AAnimal {
	
	/**
	 * The constructor doesn't need to initialize any data members
	 */
	
	public Goat()
	{
		
	}
	
	/**
	 * This method feeds the Goat food which should be of a type IFood which is any food. It then
	 * increases the happiness value of the Goat by one.
	 */
	
	public void feed( IFood food )
	{
		if ( food instanceof IFood )
		{
			improveHappiness();
		}
	}
}
